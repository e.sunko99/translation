﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
namespace Translation_Web
{

    public partial class WebForm1 : System.Web.UI.Page
    {
        string StringToCompare;
        List<string> LString = new List<string>() { "HELLO", "Program", "world", "sun", "Friends", "Архитектура", "Computer", "Error" };
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void start_Click(object sender, EventArgs e)
        {
            if (teach.Checked)
            {
                if (Sbyte.Checked)
                    TransSbyte(input.Text, PrintResult, PrintResult16);
                if (Sbyte.Checked)
                    TransSbyte(input.Text, PrintResult, PrintResult16);
                if (Short.Checked)
                    TransShort(input.Text, PrintResult, PrintResult16);
                if (Int.Checked)
                    TransInt(input.Text, PrintResult, PrintResult16);
                if (Long.Checked)
                    TransLong(input.Text, PrintResult, PrintResult16);
                if (Byte1.Checked)
                    TransByte(input.Text, PrintResult, PrintResult16);
                if (Ushort.Checked)
                    TransUshort(input.Text, PrintResult, PrintResult16);
                if (Uint.Checked)
                    TransUint(input.Text, PrintResult, PrintResult16);
                if (Ulong.Checked)
                    TransULong(input.Text, PrintResult, PrintResult16);
                if (Float.Checked)
                    TransFloat(input.Text, PrintResult, PrintResult16);
                if (Double1.Checked)
                    TransDouble(input.Text, PrintResult, PrintResult16);
                if (Decimal1.Checked)
                    TransDecimal(input.Text);
                if (String1.Checked)
                    TransString(input.Text);
                if (CiChar.Checked)
                    TransChar(input.Text, PrintResult, PrintResult16);
            }
            else
            {

                Random rnd = new Random();


                if (Sbyte.Checked)
                {
                    StringToCompare = Convert.ToString(rnd.Next(-127, 127));
                    TransSbyte(StringToCompare, PrintResult, PrintResult16);
                }

                if (Short.Checked)
                {
                    StringToCompare = Convert.ToString(rnd.Next(-100, 1300));
                    TransShort(StringToCompare, PrintResult, PrintResult16);
                }

                if (Int.Checked)
                {
                    StringToCompare = Convert.ToString(rnd.Next(-100, 1300));
                    TransInt(StringToCompare, PrintResult, PrintResult16);
                }

                if (Long.Checked)
                {
                    StringToCompare = Convert.ToString(rnd.Next(-200, 3000));
                    TransLong(StringToCompare, PrintResult, PrintResult16);
                }

                if (Byte1.Checked)
                {
                    StringToCompare = Convert.ToString(rnd.Next(0, 255));
                    TransByte(StringToCompare, PrintResult, PrintResult16);
                }

                if (Ushort.Checked)
                {
                    StringToCompare = Convert.ToString(rnd.Next(0, 1500));
                    TransUshort(StringToCompare, PrintResult, PrintResult16);
                }

                if (Uint.Checked)
                {
                    StringToCompare = Convert.ToString(rnd.Next(0, 1500));
                    TransUint(StringToCompare, PrintResult, PrintResult16);
                }

                if (Ulong.Checked)
                {
                    StringToCompare = Convert.ToString(rnd.Next(0, 1500));
                    TransULong(StringToCompare, PrintResult, PrintResult16);
                }

                if (Float.Checked)
                {
                    StringToCompare = Convert.ToString(rnd.Next(-20, 200) + rnd.Next(0, 99) / 100);
                    TransFloat(StringToCompare, PrintResult, PrintResult16);
                }

                if (Double1.Checked)
                {
                    StringToCompare = Convert.ToString(rnd.Next(-20, 200) + rnd.Next(0, 99) / 100);
                    TransDouble(StringToCompare, PrintResult, PrintResult16);
                }

                if (Decimal1.Checked)
                {
                    StringToCompare = Convert.ToString(rnd.Next(-20, 200) + rnd.Next(0, 99) / 100);
                    TransDecimal(StringToCompare);
                }

                if (String1.Checked)
                {
                    StringToCompare = LString[rnd.Next(0, LString.Count)];
                    TransString(StringToCompare);
                }

                if (CiChar.Checked)
                {
                    StringToCompare = Convert.ToString(Convert.ToChar(rnd.Next(1040, 1103)));
                    TransChar(StringToCompare, PrintResult, PrintResult16);
                }
            }
        }



        void TransSbyte(string enter, TextBox printRes, TextBox printRes16)
        {
            if (sbyte.TryParse(enter, out sbyte res))
            {
                PrintResultText(ConvertIntPoin(res), printRes, printRes16);
            }
            else
            {
                Log.Text = "Число должно быть целым и находиться в диапазоне от -128 до 127";
                Log.Text += " Ошибка ввода";
            }
        }
        string ConvertIntPoin(sbyte Number)
        {

            if (Number < 0)
                return UIntConvert("", Convert.ToString(Number + 256));
            return UIntConvert("", Convert.ToString(Number));
        }
        private string UIntConvert(string GetTwoC, string a)
        {
            int Number = Convert.ToInt32(a);
            for (int i = 7; i >= 0; i--)
            {
                GetTwoC = Number % 2 + GetTwoC;
                if (Number % 2 == 1)
                    Number--;
                Number /= 2;
            }
            return GetTwoC;

        }
        private void PrintResultText(string GetTwoC, TextBox PrintRes, TextBox PrintRes16)
        {
            PrintRes.Text = "";
            PrintRes16.Text = "";
            PrintResultFormat(GetTwoC, 8, PrintRes);
            PrintResultFormat(Translation16(GetTwoC), 2, PrintRes16);
        }
        private string Translation16(string GetTwoC)
        {
            string Result = "";
            int lk;
            for (int j = 0; j < GetTwoC.Length - 3; j += 4)
            {
                lk = 0;
                for (int r = 0; r < 4; r++)
                {
                    lk += Convert.ToInt32((GetTwoC[r + j] - 48) * Math.Pow(2, 3 - r));
                }
                Result += "0123456789ABCDEF"[lk];

            }
            return Result;
        }
        private void PrintResultFormat(string GetTwoC, int b, TextBox text)
        {
            for (int i = 0; i < GetTwoC.Length; i++)
            {
                text.Text += GetTwoC[i];
                if ((i + 1) % b == 0)
                    text.Text += " ";
            }
        }
        void TransShort(string enter, TextBox printRes, TextBox printRes16)
        {
            if (Int16.TryParse(enter, out short res))
            {
                PrintResultText(ConvertIntPoin(res), printRes, printRes16);
            }
            else
            {
                Log.Text = "Число должно быть целым и находиться в диапазоне от -32768 до 32767";
                Log.Text += " Ошибка ввода";
            }
        }

        string ConvertIntPoin(short Number)// перевод short
        {
            string Str = "";
            var tmp = Number;
            for (int i = 0; i < 2; i++)
            {
                if (Number < 0)
                    Str = UIntConvert("", Convert.ToString(tmp + 256)) + Str;
                else
                    Str = UIntConvert("", Convert.ToString(tmp)) + Str;
                tmp /= 256;
                if (Number < 0)
                    tmp += 255;
            }
            return Str;
        }
        void TransInt(string enter, TextBox printRes, TextBox printRes16)
        {
            if (Int32.TryParse(enter, out int res))
            {
                PrintResultText(ConvertIntPoin(res), printRes, printRes16);
            }
            else
            {
                Log.Text = "Число должно быть целым и находиться в диапазоне от -2147483647 до 2147483647 Повторите попытку";
                Log.Text += " Ошибка ввода";
            }
        }

        string ConvertIntPoin(int Number)// перевод int
        {
            string Str = "";
            var tmp = Number;
            for (int i = 0; i < 4; i++)
            {
                if (Number < 0)
                    Str = UIntConvert("", Convert.ToString(tmp + 256)) + Str;
                else
                    Str = UIntConvert("", Convert.ToString(tmp)) + Str;
                tmp /= 256;
                if (Number < 0)
                    tmp += 255;
            }
            return Str;
        }
        void TransLong(string enter, TextBox printRes, TextBox printRes16)
        {
            if (long.TryParse(enter, out long res))
            {
                PrintResultText(ConvertIntPoin(res), printRes, printRes16);
            }
            else
            {
                Log.Text = "Число должно быть целым Повторите попытку";
                Log.Text += " Ошибка ввода";
            }
        }

        string ConvertIntPoin(long Number)// перевод short
        {
            string Str = "";
            var tmp = Number;
            for (int i = 0; i < 8; i++)
            {
                if (Number < 0)
                    Str = UIntConvert("", Convert.ToString(tmp + 256)) + Str;
                else
                    Str = UIntConvert("", Convert.ToString(tmp)) + Str;
                tmp /= 256;
                if (Number < 0)
                    tmp += 255;
            }
            return Str;
        }
        void TransByte(string enter, TextBox printRes, TextBox printRes16)
        {
            if (byte.TryParse(enter, out byte res))
            {
                PrintResultText(ConvertUintPoin(res, 1), printRes, printRes16);
            }
            else
            {
                Log.Text = "Число должно быть целым и находиться в диапазоне от 0 до 255 Повторите попытку";
                Log.Text += " Ошибка ввода";
            }
        }

        string ConvertUintPoin(ulong Number, int y)
        {
            string Str = "";
            for (int i = y - 1; i >= 0; i--)
            {
                Str += UIntConvert("", Convert.ToString(Number >> (8 * i)));
            }
            return Str;
        }
        void TransUshort(string enter, TextBox printRes, TextBox printRes16)
        {
            if (UInt16.TryParse(enter, out ushort res))
            {
                PrintResultText(ConvertUintPoin(res, 2), printRes, printRes16);
            }
            else
            {
                Log.Text = "Число должно быть целым и находиться в диапазоне от 0 до 65535 Повторите попытку";
                Log.Text += " Ошибка ввода";
            }
        }
        void TransUint(string enter, TextBox printRes, TextBox printRes16)
        {
            if (UInt32.TryParse(enter, out uint res))
            {
                PrintResultText(ConvertUintPoin(res, 4), printRes, printRes16);
            }
            else
            {
                Log.Text = "Число должно быть целым и находиться в диапазоне от 0 до 4294967295 Повторите попытку";
                Log.Text += " Ошибка ввода";
            }
        }
        void TransULong(string enter, TextBox printRes, TextBox printRes16)
        {
            if (UInt64.TryParse(enter, out ulong res))
            {
                PrintResultText(ConvertUintPoin(res, 8), printRes, printRes16);
            }
            else
            {
                Log.Text = "Число должно быть целым и неотрицательным Повторите попытку";
                Log.Text += " Ошибка ввода";
            }
        }
        void TransFloat(string enter, TextBox printRes, TextBox printRes16)
        {
            if (Single.TryParse(enter, out float res))
            {
                PrintResultText(ConvertFloat(res), printRes, printRes16);
            }
            else
            {
                Log.Text = "Неправильно введено число Повторите попытку";
                Log.Text += " Ошибка ввода";
            }
        }


        string ConvertFloat(float Number)//переводит float
        {
            byte[] b = BitConverter.GetBytes(Number);
            string Str = "";
            for (int i = 3; i >= 0; i--)
            {
                Str += UIntConvert("", Convert.ToString(b[i]));
            }
            return Str;
        }
        void TransDouble(string enter, TextBox printRes, TextBox printRes16)
        {
            if (double.TryParse(enter, out double res))
            {
                PrintResultText(ConvertDouble(res), printRes, printRes16);
            }
            else
            {
                Log.Text = "Неправильно введено число\nПовторите попытку";
                Log.Text += " Ошибка ввода";
            }
        }
        string ConvertDouble(double Number)//переводит double
        {
            byte[] b = BitConverter.GetBytes(Number);
            string Str = "";
            for (int i = 7; i >= 0; i--)
            {
                Str += UIntConvert("", Convert.ToString(b[i]));
            }
            return Str;
        }
        void TransDecimal(string enter)
        {
            if (decimal.TryParse(enter, out decimal res))
            {
                PrintResultText(DecimalConvert(res), PrintResult, PrintResult16);
            }
            else
            {
                Log.Text = "Неправильно введено число\nПовторите попытку";
                Log.Text += " Ошибка ввода";
            }
        }
        private string DecimalConvert(decimal Number)
        {
            int[] b = decimal.GetBits(Number);
            string Str = "";
            for (int i = 3; i >= 0; i--)
            {
                byte[] tmp = BitConverter.GetBytes(b[i]);
                for (int j = 3; j >= 0; j--)
                {
                    Str += UIntConvert("", Convert.ToString(tmp[j]));
                }
            }
            return Str;

        }
        void TransString(string enter)
        {
            PrintResultText(ConvertStr(enter), PrintResult, PrintResult16);
        }
        string ConvertStr(string text)
        {
            string str = "";
            IntPtr addr;
            TypedReference tr = __makeref(text);
            unsafe
            {
                addr = **(IntPtr**)(&tr);
                byte* first = *(byte**)addr;
            }
            byte res;
            for (int i = 0; i < 14 + 2 * text.Length; i++)
            {
                unsafe
                {
                    res = *((byte*)addr + i);
                }
                str += UIntConvert("", Convert.ToString(res));
            }
            return str;
        }
        void TransChar(string enter, TextBox printRes, TextBox printRes16)
        {
            byte[] lk;
            string StrBytes;
            if (enter.Length == 1)
            {
                lk = Encoding.Unicode.GetBytes(enter);
                StrBytes = "";
                for (int i = lk.Length - 1; i >= 0; i--)
                {
                    StrBytes += UIntConvert("", Convert.ToString(lk[i]));
                }
                PrintResultText(StrBytes, printRes, printRes16);
            }
            else
            {
                Log.Text = "Введите только один символ";
                Log.Text += " Ошибка ввода";
            }
        }
        protected void btn2_Click(object sender, EventArgs e)
        {
            if (input.Text == "")
            {
                Log.Text = "Введите число,строку или символ для сравнения";
            }
            else
            {
                if (input.Text == StringToCompare)
                    Log.Text = "Правильно!";
                else
                    Log.Text = "Ошибка Правильный ответ:  " + StringToCompare;
            }
        }

        protected void teach_CheckedChanged(object sender, EventArgs e)
        {
            input.Text = "";
            btn2.Visible = test.Checked;
            if (teach.Checked)
            {
                start.Text = "Запуск";
            }
            else
            {
                start.Text = "Сгенерировать";
            }
        }
    }
}