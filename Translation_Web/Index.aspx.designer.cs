﻿//------------------------------------------------------------------------------
// <автоматически создаваемое>
//     Этот код создан программой.
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода. 
// </автоматически создаваемое>
//------------------------------------------------------------------------------

namespace Translation_Web
{


    public partial class WebForm1
    {

        /// <summary>
        /// form1 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;

        /// <summary>
        /// teach элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton teach;

        /// <summary>
        /// test элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton test;

        /// <summary>
        /// Sbyte элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton Sbyte;

        /// <summary>
        /// Short элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton Short;

        /// <summary>
        /// Int элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton Int;

        /// <summary>
        /// Long элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton Long;

        /// <summary>
        /// Float элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton Float;

        /// <summary>
        /// Double1 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton Double1;

        /// <summary>
        /// CiChar элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton CiChar;

        /// <summary>
        /// input элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox input;

        /// <summary>
        /// start элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button start;

        /// <summary>
        /// btn2 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btn2;

        /// <summary>
        /// Byte1 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton Byte1;

        /// <summary>
        /// Ushort элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton Ushort;

        /// <summary>
        /// Uint элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton Uint;

        /// <summary>
        /// Ulong элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton Ulong;

        /// <summary>
        /// Decimal1 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton Decimal1;

        /// <summary>
        /// String1 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton String1;

        /// <summary>
        /// PrintResult элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox PrintResult;

        /// <summary>
        /// PrintResult16 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox PrintResult16;

        /// <summary>
        /// Log элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox Log;
    }
}
