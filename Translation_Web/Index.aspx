﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Translation_Web.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Представление данных во внутреннем формате</title>
</head>
<body>
    <form id="form1" runat="server">
        
        <asp:Table runat="server" CellPadding="10" HorizontalAlign="Center" Width="90%">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell></asp:TableHeaderCell>
                <asp:TableHeaderCell>Целочисленные типы</asp:TableHeaderCell>
                <asp:TableHeaderCell>Вещественные типы</asp:TableHeaderCell>
                <asp:TableHeaderCell></asp:TableHeaderCell>
            </asp:TableHeaderRow>
            <asp:TableRow>
                <asp:TableCell>
                    <p>Выберите режим</p>
                    <asp:RadioButton GroupName="mode" ID="teach" value="teach" Checked="true" runat="server" OnCheckedChanged="teach_CheckedChanged" AutoPostBack="true" />
                    <label for="teach">Обучение</label>
                    <br />
                    <asp:RadioButton GroupName="mode" ID="test" value="test" runat="server" OnCheckedChanged="teach_CheckedChanged" AutoPostBack="true" />
                    <label for="test">Контроль</label>
                </asp:TableCell>
                <asp:TableCell>
                    Знаковые
                        <br />
                    <asp:RadioButton GroupName="type" ID="Sbyte" value="sbyte" Checked="true" runat="server" />
                    <label for="Sbyte">sbyte</label>
                    <br />
                    <asp:RadioButton GroupName="type" ID="Short" value="short" runat="server" />
                    <label for="Short">short</label>
                    <br />
                    <asp:RadioButton GroupName="type" ID="Int" value="int" runat="server" />
                    <label for="Int">int</label>
                    <br />
                    <asp:RadioButton GroupName="type" ID="Long" value="long" runat="server" />
                    <label for="Long">long</label>
                </asp:TableCell>
                <asp:TableCell>
                    С плавающей точкой
                        <br />
                    <asp:RadioButton GroupName="type" ID="Float" value="float" runat="server" />
                    <label for="Float">float</label>
                    <br />
                    <asp:RadioButton GroupName="type" ID="Double1" value="double" runat="server" />
                    <label for="Double1">double</label>
                </asp:TableCell>
                <asp:TableCell>
                    Символьный тип
                        <br />
                    <asp:RadioButton GroupName="type" ID="CiChar" value="char" runat="server" />
                    <label for="CiChar">char</label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    Ввод
                        <br />
                    <asp:TextBox ID="input" runat="server" Width="100%"></asp:TextBox><br />
                    <asp:Button ID="start" OnClick="start_Click" runat="server" Text="Запуск" />
                    <asp:Button ID="btn2" OnClick="btn2_Click" runat="server" Text="Запуск" Visible="false"/>
                </asp:TableCell>
                <asp:TableCell>
                    Беззнаковые
                        <br />
                    <asp:RadioButton GroupName="type" ID="Byte1" value="byte" runat="server" />
                    <label for="Byte1">byte</label>
                    <br />
                    <asp:RadioButton GroupName="type" ID="Ushort" value="ushort" runat="server" />
                    <label for="Ushort">ushort</label>
                    <br />
                    <asp:RadioButton GroupName="type" ID="Uint" value="uint" runat="server" />
                    <label for="Uint">uint</label>
                    <br />
                    <asp:RadioButton GroupName="type" ID="Ulong" value="ulong" runat="server" />
                    <label for="Ulong">ulong</label>
                </asp:TableCell>
                <asp:TableCell>
                    С фиксированной точкой
                        <br />
                    <asp:RadioButton GroupName="type" ID="Decimal1" value="decimal" runat="server" />
                    <label for="Decimal1">decimal</label>
                </asp:TableCell>
                <asp:TableCell>
                    Символьный тип
                        <br />
                    <asp:RadioButton GroupName="type" ID="String1" value="string" runat="server" />
                    <label for="String1">string</label>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <p align="center">Двоичное представление</p>
        <asp:TextBox ID="PrintResult" runat="server" Width="100%"></asp:TextBox>
        <p align="center">Шестнадцатиричное представление</p>
        <asp:TextBox ID="PrintResult16" runat="server" Width="100%"></asp:TextBox>
        <asp:TextBox ID="Log" runat="server" Width="100%"></asp:TextBox>
    </form>
</body>
</html>
