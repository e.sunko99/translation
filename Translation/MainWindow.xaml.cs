﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
namespace Translation
{
    public partial class MainWindow : Window
    {
        string NameCurrentRAdioButton = "";
        string NameCurrentRAdioButtonP = "";
        string StringToCompare = "";
        private List<RadioButton> ListRadioButtonCi;
        List<RadioButton> ListRadioButtonPas;
        double k1 = 0;// запоминает остаток при переводе в вещественного числа для дальнейшего перевода и увеличении точности
        List<string> LString = new List<string>() { "HELLO", "Program", "world", "sun", "Friends", "Архитектура", "Computer", "Error" };
        public MainWindow()
        {
            InitializeComponent();
            ListRadioButtonCi = new List<RadioButton>() { Sbyte, Uint, Ushort, Ulong, Byte, Short, Long, Int, Float, Double, Decimal1, RbCiChar, String1 };
            ListRadioButtonPas = new List<RadioButton>() { PShortInt, PSmallInt, PLongInt, PInt64, PByte, PWorld, PSingle, PLongWorld, PExtendet, Pcurrency, RbPAnsiChar, RbPAnsiString, RbPChar, RbPPChar, RbPWideChar, RbPWideString, RbPReal };
        }
        //выбор режима обучения
        private void TrainingMode_Checked(object sender, RoutedEventArgs e)
        {
            NameCurrentRAdioButton = null;
            SelectionCheck1();
            UnChekedRadBut(ListRadioButtonCi);
            GBEnter.Visibility = Visibility;
            Check.Visibility = Visibility.Hidden;
            Perevod.Visibility = Visibility;
            Clear();
            EnterNum.Clear();
            CiGeneric.Visibility = Visibility.Collapsed;
        }
        void Clear()//очищает поля вывода и ввода
        {
            PrintResult.Clear();
            PrintResult16.Clear();
        }
        private void SelectionCheck1()//разрешает выбор типа данных
        {
            CiSign.IsEnabled = true;
            CiUSign.IsEnabled = true;
            CiReal.IsEnabled = true;
            CiRealF.IsEnabled = true;
            CiChar.IsEnabled = true;
            CiString.IsEnabled = true;
        }
        //происходит при выборе radiobutton
        private void RadButCi_Checked(object sender, RoutedEventArgs e)
        {
            Clear();
            NameCurrentRAdioButton = ((RadioButton)sender).Name;
            ListRadioButtonCi.Remove((RadioButton)sender);
            UnChekedRadBut(ListRadioButtonCi);
            ListRadioButtonCi.Add((RadioButton)sender);
            if (ControlMode.IsChecked == true)
            {
                GBEnter.Visibility = Visibility.Hidden;
                CiGeneric.Visibility = Visibility;
            }
        }
        //сбрасывает выбор в других radiobutton
        void UnChekedRadBut(List<RadioButton> radioButtons)
        {
            foreach (var item in radioButtons)
            {
                item.IsChecked = false;
            }
        }
        private void Perevod_Click(object sender, RoutedEventArgs e)
        {
            switch (NameCurrentRAdioButton)
            {
                case "":
                    MessageBox.Show("Выберите тип данных");
                    break;
                case "Sbyte":
                    TransSbyte(EnterNum.Text, PrintResult, PrintResult16);
                    break;
                case "Short":
                    TransShort(EnterNum.Text, PrintResult, PrintResult16);
                    break;
                case "Int":
                    TransInt(EnterNum.Text, PrintResult, PrintResult16);
                    break;
                case "Long":
                    TransLong(EnterNum.Text, PrintResult, PrintResult16);
                    break;
                case "Byte":
                    TransByte(EnterNum.Text, PrintResult, PrintResult16);
                    break;
                case "Ushort":
                    TransUshort(EnterNum.Text, PrintResult, PrintResult16);
                    break;
                case "Uint":
                    TransUint(EnterNum.Text, PrintResult, PrintResult16);
                    break;
                case "Ulong":
                    TransULong(EnterNum.Text, PrintResult, PrintResult16);
                    break;
                case "Float":
                    TransFloat(EnterNum.Text, PrintResult, PrintResult16);
                    break;
                case "Double":
                    TransDouble(EnterNum.Text, PrintResult, PrintResult16);
                    break;
                case "Decimal1":
                    TransDecimal(EnterNum.Text);
                    break;
                case "String1":
                    TransString(EnterNum.Text);
                    break;
                case "RbCiChar":
                    TransChar(EnterNum.Text, PrintResult, PrintResult16);
                    break;
            }
        }
        void TransSbyte(string enter, TextBox printRes, TextBox printRes16)
        {
            if (sbyte.TryParse(enter, out sbyte res))
            {
                PrintResultText(ConvertIntPoin(res), printRes, printRes16);
            }
            else
            {
                MessageBox.Show("Число должно быть целым и находиться в диапазоне от -128 до 127", "Ошибка ввода");
            }
        }
        // перевод sbyte
        string ConvertIntPoin(sbyte Number)
        {
            string Str = "";
            unsafe
            {
                sbyte* p;
                p = &Number;
                uint addr = (uint)p;
                byte* bytePointer = (byte*)addr;
                Str += UIntConvert("", Convert.ToString(*bytePointer));
            }
            return Str;
        }
        //перевод в 2-ную систему счисления
        private string UIntConvert(string GetTwoC, string a)
        {
            int Number = Convert.ToInt32(a);
            for (int i = 7; i >= 0; i--)
            {
                GetTwoC = Number % 2 + GetTwoC;
                if (Number % 2 == 1)
                    Number--;
                Number /= 2;
            }
            return GetTwoC;
        }
        // вывод результата в соответствующем виде
        private void PrintResultText(string GetTwoC, TextBox PrintRes, TextBox PrintRes16)
        {
            PrintRes.Clear();
            PrintRes16.Clear();
            PrintResultFormat(GetTwoC, 8, PrintRes);
            PrintResultFormat(Translation16(GetTwoC), 2, PrintRes16);
        }
        // переводит в 16-систему счисления
        private string Translation16(string GetTwoC)
        {
            string Result = "";
            int lk = 0;
            for (int j = 0; j < GetTwoC.Length - 3; j += 4)
            {
                lk = 0;
                for (int r = 0; r < 4; r++)
                {
                    lk += Convert.ToInt32(((GetTwoC[r + j]) - 48) * Math.Pow(2, 3 - r));
                }
                if (lk < 10)
                    Result += lk.ToString();
                if (lk == 10)
                    Result += "A";
                if (lk == 11)
                    Result += "B";
                if (lk == 12)
                    Result += "C";
                if (lk == 13)
                    Result += "D";
                if (lk == 14)
                    Result += "E";
                if (lk == 15)
                    Result += "F";
            }
            return Result;
        }
        //изменяет строку в формат -0000- или -00-
        private void PrintResultFormat(string GetTwoC, int b, TextBox text)
        {
            for (int i = 0; i < GetTwoC.Length; i++)
            {
                text.Text += GetTwoC[i];
                if ((i + 1) % b == 0)
                    text.Text += " ";
            }
        }
        void TransShort(string enter, TextBox printRes, TextBox printRes16)
        {
            if (Int16.TryParse(enter, out short res))
            {
                PrintResultText(ConvertIntPoin(res), printRes, printRes16);
            }
            else
            {
                MessageBox.Show("Число должно быть целым и находиться в диапазоне от -32768 до 32767", "Ошибка ввода");
            }
        }
        string ConvertIntPoin(short Number)// перевод short
        {
            string Str = "";
            unsafe
            {
                short* p;
                p = &Number;
                uint addr = (uint)p;
                for (int i = 1; i >= 0; i--)
                {
                    byte* bytePointer = (byte*)addr + i;
                    Str += UIntConvert("", Convert.ToString(*bytePointer));
                }
            }
            return Str;
        }
        void TransInt(string enter, TextBox printRes, TextBox printRes16)
        {
            if (Int32.TryParse(enter, out int res))
            {
                PrintResultText(ConvertIntPoin(res), printRes, printRes16);
            }
            else
            {
                MessageBox.Show("Число должно быть целым и находиться в диапазоне от -2147483647 до 2147483647\nПовторите попытку", "Ошибка ввода");
            }
        }
        string ConvertIntPoin(int Number)//перевод int
        {
            string Str = "";
            unsafe
            {
                int* p;
                p = &Number;
                uint addr = (uint)p;
                for (int i = 3; i >= 0; i--)
                {
                    byte* bytePointer = (byte*)addr + i;
                    Str += UIntConvert("", Convert.ToString(*bytePointer));
                }
            }
            return Str;
        }
        void TransLong(string enter, TextBox printRes, TextBox printRes16)
        {
            if (long.TryParse(enter, out long res))
            {
                PrintResultText(ConvertIntPoin(res), printRes, printRes16);
            }
            else
            {
                MessageBox.Show("Число должно быть целым\nПовторите попытку", "Ошибка ввода");
            }
        }
        string ConvertIntPoin(long Number)//перевод long
        {
            string Str = "";
            unsafe
            {
                long* p;
                p = &Number;
                uint addr = (uint)p;
                for (int i = 7; i >= 0; i--)
                {
                    byte* bytePointer = (byte*)addr + i;
                    Str += UIntConvert("", Convert.ToString(*bytePointer));
                }
            }
            return Str;
        }
        void TransByte(string enter, TextBox printRes, TextBox printRes16)
        {
            if (byte.TryParse(enter, out byte res))
            {
                PrintResultText(ConvertUintPoin(res, 1), printRes, printRes16);
            }
            else
            {
                MessageBox.Show("Число должно быть целым и находиться в диапазоне от 0 до 255\nПовторите попытку", "Ошибка ввода");
            }
        }
        //переводит безнаковые целые числа
        string ConvertUintPoin(ulong Number, int y)
        {
            string Str = "";
            unsafe
            {
                ulong* p;
                p = &Number;
                uint addr = (uint)p;
                for (int i = y - 1; i >= 0; i--)
                {
                    byte* bytePointer = (byte*)addr + i;
                    Str += UIntConvert("", Convert.ToString(*bytePointer));
                }
            }
            return Str;
        }
        void TransUshort(string enter, TextBox printRes, TextBox printRes16)
        {
            if (UInt16.TryParse(enter, out ushort res))
            {
                PrintResultText(ConvertUintPoin(res, 2), printRes, printRes16);
            }
            else
            {
                MessageBox.Show("Число должно быть целым и находиться в диапазоне от 0 до 65535\nПовторите попытку", "Ошибка ввода");
            }
        }
        void TransUint(string enter, TextBox printRes, TextBox printRes16)
        {
            if (UInt32.TryParse(enter, out uint res))
            {
                PrintResultText(ConvertUintPoin(res, 4), printRes, printRes16);
            }
            else
            {
                MessageBox.Show("Число должно быть целым и находиться в диапазоне от 0 до 4294967295\nПовторите попытку", "Ошибка ввода");
            }
        }
        void TransULong(string enter, TextBox printRes, TextBox printRes16)
        {
            if (UInt64.TryParse(enter, out ulong res))
            {
                PrintResultText(ConvertUintPoin(res, 8), printRes, printRes16);
            }
            else
            {
                MessageBox.Show("Число должно быть целым и неотрицательным\nПовторите попытку", "Ошибка ввода");
            }
        }
        void TransFloat(string enter, TextBox printRes, TextBox printRes16)
        {
            if (Single.TryParse(enter, out float res))
            {
                PrintResultText(ConvertFloat(res), printRes, printRes16);
            }
            else
            {
                MessageBox.Show("Неправильно введено число\nПовторите попытку", "Ошибка ввода");
            }
        }
        string ConvertFloat(float Number)//переводит float
        {
            string Str = "";
            unsafe
            {
                float* p;
                p = &Number;
                uint addr = (uint)p;
                for (int i = 3; i >= 0; i--)
                {
                    byte* bytePointer = (byte*)addr + i;
                    Str += UIntConvert("", Convert.ToString(*bytePointer));
                }
            }
            return Str;
        }
        void TransDouble(string enter, TextBox printRes, TextBox printRes16)
        {
            if (double.TryParse(enter, out double res))
            {
                PrintResultText(ConvertDouble(res), printRes, printRes16);
            }
            else
            {
                MessageBox.Show("Неправильно введено число\nПовторите попытку", "Ошибка ввода");
            }
        }
        string ConvertDouble(double Number)//переводит double
        {
            string Str = "";
            unsafe
            {
                double* p;
                p = &Number;
                uint addr = (uint)p;
                for (int i = 7; i >= 0; i--)
                {
                    byte* bytePointer = (byte*)addr + i;
                    Str += UIntConvert("", Convert.ToString(*bytePointer));
                }
            }
            return Str;
        }
        void TransDecimal(string enter)
        {
            if (decimal.TryParse(enter, out decimal res))
            {
                PrintResultText(DecimalConvert(res), PrintResult, PrintResult16);
            }
            else
            {
                MessageBox.Show("Неправильно введено число\nПовторите попытку", "Ошибка ввода");
            }
        }
        private string DecimalConvert(decimal Number)
        {
            string Str = "";
            unsafe
            {
                decimal* p;
                p = &Number;
                uint addr = (uint)p;
                int[] mn = new int[] { 3, 7, 15, 11 };
                for (int y = 0; y < 4; y++)
                {
                    for (int i = mn[y]; i >= mn[y] - 3; i--)
                    {
                        byte* bytePointer = (byte*)addr + i;
                        Str += UIntConvert("", Convert.ToString(*bytePointer));
                    }
                }
            }
            return Str;
        }
        void TransString(string enter)
        {
            PrintResultText(ConvertStr(enter), PrintResult, PrintResult16);
        }
        string ConvertStr(string text)
        {
            string str = "";
            IntPtr addr;
            TypedReference tr = __makeref(text);
            unsafe
            {
                addr = **(IntPtr**)(&tr);
                byte* first = *(byte**)addr;
            }
            byte res;
            for (int i = 0; i < 14 + 2 * text.Length; i++)
            {
                unsafe
                {
                    res = *((byte*)addr + i);
                }
                str += UIntConvert("", Convert.ToString(res));
            }
            return str;
        }
        void TransChar(string enter, TextBox printRes, TextBox printRes16)
        {
            byte[] lk;
            string StrBytes;
            if (enter.Length == 1)
            {
                lk = Encoding.Unicode.GetBytes(enter);
                StrBytes = "";
                for (int i = lk.Length - 1; i >= 0; i--)
                {
                    StrBytes += UIntConvert("", Convert.ToString(lk[i]));
                }
                PrintResultText(StrBytes, printRes, printRes16);
            }
            else
            {
                MessageBox.Show("Введите только один символ", "Ошибка ввода");
            }
        }
        private void ControlMode_Checked(object sender, RoutedEventArgs e)
        {
            NameCurrentRAdioButton = null;
            EnterNum.Clear();
            Clear();
            GBEnter.Visibility = Visibility.Collapsed;
            SelectionCheck1();
            UnChekedRadBut(ListRadioButtonCi);
            CiGeneric.Visibility = Visibility;
            StringToCompare = null;
        }
        private void CiGeneric_Click(object sender, RoutedEventArgs e)
        {
            Random rnd = new Random();
            if (NameCurrentRAdioButton == null)
            {
                MessageBox.Show("Выберите тип данных", "Ошибка");
            }
            else
            {
                CiGeneric.Visibility = Visibility.Hidden;
                GBEnter.Visibility = Visibility;
                Check.Visibility = Visibility;
                Perevod.Visibility = Visibility.Hidden;
                EnterNum.Clear();
                switch (NameCurrentRAdioButton)
                {
                    case "Sbyte":
                        StringToCompare = Convert.ToString(rnd.Next(-127, 127));
                        TransSbyte(StringToCompare, PrintResult, PrintResult16);
                        break;
                    case "Short":
                        StringToCompare = Convert.ToString(rnd.Next(-100, 1300));
                        TransShort(StringToCompare, PrintResult, PrintResult16);
                        break;
                    case "Int":
                        StringToCompare = Convert.ToString(rnd.Next(-100, 1300));
                        TransInt(StringToCompare, PrintResult, PrintResult16);
                        break;
                    case "Long":
                        StringToCompare = Convert.ToString(rnd.Next(-200, 3000));
                        TransLong(StringToCompare, PrintResult, PrintResult16);
                        break;
                    case "Byte":
                        StringToCompare = Convert.ToString(rnd.Next(0, 255));
                        TransByte(StringToCompare, PrintResult, PrintResult16);
                        break;
                    case "Ushort":
                        StringToCompare = Convert.ToString(rnd.Next(0, 1500));
                        TransUshort(StringToCompare, PrintResult, PrintResult16);
                        break;
                    case "Uint":
                        StringToCompare = Convert.ToString(rnd.Next(0, 1500));
                        TransUint(StringToCompare, PrintResult, PrintResult16);
                        break;
                    case "Ulong":
                        StringToCompare = Convert.ToString(rnd.Next(0, 1500));
                        TransULong(StringToCompare, PrintResult, PrintResult16);
                        break;
                    case "Float":
                        StringToCompare = Convert.ToString(rnd.Next(-20, 200) + rnd.Next(0, 99) / 100);
                        TransFloat(StringToCompare, PrintResult, PrintResult16);
                        break;
                    case "Double":
                        StringToCompare = Convert.ToString(rnd.Next(-20, 200) + rnd.Next(0, 99) / 100);
                        TransDouble(StringToCompare, PrintResult, PrintResult16);
                        break;
                    case "Decimal1":
                        StringToCompare = Convert.ToString(rnd.Next(-20, 200) + rnd.Next(0, 99) / 100);
                        TransDecimal(StringToCompare);
                        break;
                    case "String1":
                        StringToCompare = LString[rnd.Next(0, LString.Count)];
                        TransString(StringToCompare);
                        break;
                    case "RbCiChar":
                        StringToCompare = Convert.ToString(Convert.ToChar(rnd.Next(1040, 1103)));
                        TransChar(StringToCompare, PrintResult, PrintResult16);
                        break;
                }
            }
        }
        //Pascal
        private void PShortInt_Checked(object sender, RoutedEventArgs e)
        {
            PClear();
            NameCurrentRAdioButtonP = ((RadioButton)sender).Name;
            ListRadioButtonPas.Remove((RadioButton)sender);
            UnChekedRadBut(ListRadioButtonPas);
            ListRadioButtonPas.Add((RadioButton)sender);
            if (PControlMode.IsChecked == true)
            {
                PGBEnter.Visibility = Visibility.Hidden;
                PGeneric.Visibility = Visibility;
            }
        }
        private void PPerevod_Click(object sender, RoutedEventArgs e)
        {
            switch (NameCurrentRAdioButtonP)
            {
                case "":
                    MessageBox.Show("Выберите тип данных");
                    break;
                case "PShortInt":
                    TransSbyte(PEnterNum.Text, PPrintResult, PPrintResult16);
                    break;
                case "PSmallInt":
                    TransShort(PEnterNum.Text, PPrintResult, PPrintResult16);
                    break;
                case "PLongInt":
                    TransInt(PEnterNum.Text, PPrintResult, PPrintResult16);
                    break;
                case "PInt64":
                    TransLong(PEnterNum.Text, PPrintResult, PPrintResult16);
                    break;
                case "PByte":
                    TransByte(PEnterNum.Text, PPrintResult, PPrintResult16);
                    break;
                case "PWorld":
                    TransUshort(PEnterNum.Text, PPrintResult, PPrintResult16);
                    break;
                case "PLongWorld":
                    TransUint(PEnterNum.Text, PPrintResult, PPrintResult16);
                    break;
                case "PSingle":
                    TransFloat(PEnterNum.Text, PPrintResult, PPrintResult16);
                    break;
                case "RbPReal":
                    TransDouble(PEnterNum.Text, PPrintResult, PPrintResult16);
                    break;
                case "PExtendet":
                    TransExtendet(PEnterNum.Text, PPrintResult, PPrintResult16);
                    break;
                case "Pcurrency":
                    TransCurrency(PEnterNum.Text, PPrintResult, PPrintResult16);
                    break;
                case "RbPChar":
                    TransCharPas(PEnterNum.Text, PPrintResult, PPrintResult16);
                    break;
                case "RbPAnsiChar":
                    TransAnsiChar(PEnterNum.Text, PPrintResult, PPrintResult16);
                    break;
                case "RbPWideChar":
                    TransChar(PEnterNum.Text, PPrintResult, PPrintResult16);
                    break;
                case "RbPPChar":
                    TransPPChar(PEnterNum.Text, PPrintResult, PPrintResult16);
                    break;
                case "RbPAnsiString":
                    TransAnsiString(PEnterNum.Text, PPrintResult, PPrintResult16);
                    break;
                case "RbPWideString":
                    TransWideString(PEnterNum.Text, PPrintResult, PPrintResult16);
                    break;
            }
        }
        void TransExtendet(string Enter, TextBox PrintRes, TextBox PrintRes16)
        {
            if (double.TryParse(Enter, out double res))
            {
                PrintResultText(ConvertReal(res, 15, 63, 16383), PrintRes, PrintRes16);
            }
            else
            {
                MessageBox.Show("Неправильно введено число\nПовторите попытку", "Ошибка ввода");
            }
        }
        private string ConvertReal(double Number, byte eCount, byte fCount, int Const)
        {
            string S1 = "", S2 = "";
            string E = "";
            ulong a = Convert.ToUInt64(Math.Abs(Math.Truncate(Number)));
            double b = Number - Math.Truncate(Number);
            sbyte Order;
            if (a == 0)
            {
                S2 = TranslationFraction(S2, S1.Length, b, eCount);
                //поиск позициции
                Order = -1;
                while (S2[Order + 1] != '1')
                    Order++;
                S2 = S2.Remove(0, Order + 2);
                S2 = TranslationFraction(S2, fCount - 2 - Order, k1, fCount);
                E = UIntConvert(E, eCount, Convert.ToUInt64(Const - 2 - Order));
            }
            else
            {
                while (a >= 2)
                {
                    S1 = Convert.ToString(a % 2) + S1;//без единицы
                    a /= 2;
                }
                Order = Convert.ToSByte(S1.Length);
                S2 = TranslationFraction(S2, S1.Length, b, fCount);
                E = UIntConvert(E, eCount, Convert.ToUInt64(Const + Order));
            }
            string Str;
            if (Number >= 0) Str = "0";
            else Str = "1";
            for (int s = 0; s < E.Length; s++)
            {
                Str += E[s];
            }
            Str += "1" + S1 + S2;
            return Str;
        }
        //перевод дробной части
        private string TranslationFraction(string S2, int y, double b, byte fCount)
        {
            for (int i = 0; i < (fCount - y); i++)
            {
                if ((2 * b) > 1)
                {
                    S2 += Convert.ToString(Convert.ToInt32(Math.Truncate(b * 2)));
                    b = Convert.ToDouble(b * 2) - Math.Truncate(b * 2);
                }
                else
                {
                    if (((2 * b) < 1))
                    {
                        S2 += "0";
                        b = Convert.ToDouble(b * 2);
                    }
                    else
                    {
                        if (2 * b == 1)
                            S2 += "1";
                        if ((2 * b) == 0)
                            S2 += "0";
                        b = 0;
                    }
                }
            }
            k1 = b;
            return S2;
        }
        //переводит безнаковые целые числа
        private string UIntConvert(string GetTwoC, int BCount, ulong Number)
        {
            for (int i = BCount - 1; i >= 0; i--)
            {
                GetTwoC = Number % 2 + GetTwoC;
                if (Number % 2 == 1)
                    Number--;
                Number /= 2;
            }
            return GetTwoC;
        }
        void TransCurrency(string Enter, TextBox PrintRes, TextBox PrintRes16)
        {
            if (double.TryParse(Enter, out double res))
            {
                res = Math.Round(res, 4, MidpointRounding.AwayFromZero) * 10000;
                PrintResultText(ConvertCur(Convert.ToInt64(res), 64), PrintRes, PrintRes16);
            }
            else
            {
                MessageBox.Show("Неправильно введено число\nПовторите попытку", "Ошибка ввода");
            }
        }
        string ConvertCur(long Number, byte BCount)
        {
            string GetTwoC = "";
            if (Number >= 0)
            {
                GetTwoC = UIntConvert(GetTwoC, BCount, Convert.ToUInt64(Number));
            }
            else
            {
                Number /= -1;
                bool kl = true;
                for (int i = BCount - 1; i > 0; i--)
                {
                    if (kl)
                    {
                        if (Number % 2 == 1)
                            kl = false;
                        GetTwoC = Number % 2 + GetTwoC;
                    }
                    else
                    {
                        if (Number % 2 == 1)
                            GetTwoC = 0 + GetTwoC;
                        else
                            GetTwoC = 1 + GetTwoC;
                    }
                    if (Number % 2 == 1)
                        Number--;
                    Number /= 2;
                }
                GetTwoC = 1 + GetTwoC;
            }
            return GetTwoC;
        }
        void TransCharPas(string Enter, TextBox PrintRes, TextBox PrintRes16)
        {
            byte[] lk;
            string StrBytes;
            if (Enter.Length == 1)
            {
                lk = Encoding.GetEncoding(866).GetBytes(Enter);
                StrBytes = UIntConvert("", Convert.ToString(lk[0]));
                PrintResultText(StrBytes, PrintRes, PrintRes16);
            }
            else
            {
                MessageBox.Show("Введите только один символ", "Ошибка ввода");
            }
        }
        void TransAnsiChar(string Enter, TextBox PrintRes, TextBox PrintRes16)
        {
            byte[] lk;
            string StrBytes;
            if (Enter.Length == 1)
            {
                lk = Encoding.Default.GetBytes(Enter);
                StrBytes = UIntConvert("", Convert.ToString(lk[0]));
                PrintResultText(StrBytes, PrintRes, PrintRes16);
            }
            else
            {
                MessageBox.Show("Введите только один символ", "Ошибка ввода");
            }
        }
        void TransPPChar(string Enter, TextBox PrintRes, TextBox PrintRes16)
        {
            string StrBytes;
            byte[] lk;
            lk = Encoding.GetEncoding(866).GetBytes(Enter);
            StrBytes = "";
            for (int i = 0; i < lk.Length; i++)
            {
                StrBytes += UIntConvert("", Convert.ToString(lk[i]));
            }
            PrintResultText(StrBytes + "00000000", PrintRes, PrintRes16);
        }
        void TransAnsiString(string Enter, TextBox PrintRes, TextBox PrintRes16)
        {
            string StrBytes;
            byte[] lk;
            lk = Encoding.Default.GetBytes(Enter);
            StrBytes = "";
            for (int i = 0; i < lk.Length; i++)
            {
                StrBytes += UIntConvert("", Convert.ToString(lk[i]));
            }
            PrintResultText(StrBytes + "00000000", PrintRes, PrintRes16);
        }
        void TransWideString(string Enter, TextBox PrintRes, TextBox PrintRes16)
        {
            string StrBytes;
            byte[] lk;
            lk = Encoding.Unicode.GetBytes(Enter);
            StrBytes = "";
            for (int i = 0; i < lk.Length; i++)
            {
                StrBytes += UIntConvert("", Convert.ToString(lk[i]));
            }
            PrintResultText(StrBytes + "00000000", PrintRes, PrintRes16);
        }
        private void Check_Click(object sender, RoutedEventArgs e)
        {
            if (EnterNum.Text == "")
            {
                MessageBox.Show("Введите число,строку или символ для сравнения");
            }
            else
            {
                if (EnterNum.Text == StringToCompare)
                    MessageBox.Show("Правильно!");
                else
                    MessageBox.Show("Ошибка\nПравильный ответ: \"" + StringToCompare + "\"");
            }
        }
        private void PCheck_Click(object sender, RoutedEventArgs e)
        {
            if (PEnterNum.Text == "")
            {
                MessageBox.Show("Введите число,строку или символ для сравнения");
            }
            else
            {
                if (PEnterNum.Text == StringToCompare)
                    MessageBox.Show("Правильно!");
                else
                    MessageBox.Show("Ошибка\nПравильный ответ: \"" + StringToCompare + "\"");
            }
        }
        private void PGeneric_Click(object sender, RoutedEventArgs e)
        {
            Random rnd = new Random();
            if (NameCurrentRAdioButton == null)
            {
                MessageBox.Show("Выберите тип данных", "Ошибка");
            }
            else
            {
                PGeneric.Visibility = Visibility.Hidden;
                PGBEnter.Visibility = Visibility;
                PCheck.Visibility = Visibility;
                PPerevod.Visibility = Visibility.Hidden;
                PEnterNum.Clear();
                switch (NameCurrentRAdioButtonP)
                {
                    case "":
                        MessageBox.Show("Выберите тип данных");
                        break;
                    case "PShortInt":
                        StringToCompare = Convert.ToString(rnd.Next(-100, 1300));
                        TransSbyte(StringToCompare, PPrintResult, PPrintResult16);
                        break;
                    case "PSmallInt":
                        StringToCompare = Convert.ToString(rnd.Next(-100, 1300));
                        TransShort(StringToCompare, PPrintResult, PPrintResult16);
                        break;
                    case "PLongInt":
                        StringToCompare = Convert.ToString(rnd.Next(-100, 1300));
                        TransInt(StringToCompare, PPrintResult, PPrintResult16);
                        break;
                    case "PInt64":
                        StringToCompare = Convert.ToString(rnd.Next(-200, 3000));
                        TransLong(StringToCompare, PPrintResult, PPrintResult16);
                        break;
                    case "PByte":
                        StringToCompare = Convert.ToString(rnd.Next(0, 255));
                        TransByte(StringToCompare, PPrintResult, PPrintResult16);
                        break;
                    case "PWorld":
                        StringToCompare = Convert.ToString(rnd.Next(0, 1500));
                        TransUshort(StringToCompare, PPrintResult, PPrintResult16);
                        break;
                    case "PLongWorld":
                        StringToCompare = Convert.ToString(rnd.Next(0, 1500));
                        TransUint(StringToCompare, PPrintResult, PPrintResult16);
                        break;
                    case "PSingle":
                        StringToCompare = Convert.ToString(rnd.Next(-20, 200) + rnd.Next(0, 99) / 100);
                        TransFloat(StringToCompare, PPrintResult, PPrintResult16);
                        break;
                    case "RbPReal":
                        StringToCompare = Convert.ToString(rnd.Next(-20, 200) + rnd.Next(0, 99) / 100);
                        TransDouble(StringToCompare, PPrintResult, PPrintResult16);
                        break;
                    case "PExtendet":
                        StringToCompare = Convert.ToString(rnd.Next(-20, 200) + rnd.Next(0, 99) / 100);
                        TransExtendet(StringToCompare, PPrintResult, PPrintResult16);
                        break;
                    case "Pcurrency":
                        StringToCompare = Convert.ToString(rnd.Next(-20, 200) + rnd.Next(0, 99) / 100);
                        TransCurrency(StringToCompare, PPrintResult, PPrintResult16);
                        break;
                    case "RbPChar":
                        StringToCompare = Convert.ToString(Convert.ToChar(rnd.Next(49, 125)));
                        TransCharPas(StringToCompare, PPrintResult, PPrintResult16);
                        break;
                    case "RbPAnsiChar":
                        StringToCompare = Convert.ToString(Convert.ToChar(rnd.Next(49, 125)));
                        TransAnsiChar(StringToCompare, PPrintResult, PPrintResult16);
                        break;
                    case "RbPWideChar":
                        StringToCompare = Convert.ToString(Convert.ToChar(rnd.Next(1040, 1103)));
                        TransChar(StringToCompare, PPrintResult, PPrintResult16);
                        break;
                    case "RbPPChar":
                        StringToCompare = LString[rnd.Next(0, LString.Count)];
                        TransPPChar(StringToCompare, PPrintResult, PPrintResult16);
                        break;
                    case "RbPAnsiString":
                        StringToCompare = LString[rnd.Next(0, LString.Count)];
                        TransAnsiString(StringToCompare, PPrintResult, PPrintResult16);
                        break;
                    case "RbPWideString":
                        StringToCompare = LString[rnd.Next(0, LString.Count)];
                        TransWideString(StringToCompare, PPrintResult, PPrintResult16);
                        break;
                }
            }
        }
        //информация о разработчике
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Пинчук Анастасия.Группа-БИСТ-211");
        }
        //вывод руководства пользователя
        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            Window1 GetWindow1 = new Window1();
            GetWindow1.Show();
        }
        // информация о приложении
        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Данное приложение является курсовым проектом по дисциплине: Операционные системы 2019 г.©", "Информация о приложении", MessageBoxButton.OK, MessageBoxImage.Asterisk);
        }
        private void PTrainingMode_Checked(object sender, RoutedEventArgs e)
        {
            NameCurrentRAdioButtonP = null;
            PSelectionCheck1();
            UnChekedRadBut(ListRadioButtonPas);
            PGBEnter.Visibility = Visibility;
            PCheck.Visibility = Visibility.Hidden;
            PPerevod.Visibility = Visibility;
            PClear();
            PEnterNum.Clear();
            PGeneric.Visibility = Visibility.Collapsed;
        }
        private void PControlMode_Checked(object sender, RoutedEventArgs e)
        {
            NameCurrentRAdioButtonP = null;
            PEnterNum.Clear();
            PClear();
            PGBEnter.Visibility = Visibility.Collapsed;
            PSelectionCheck1();
            UnChekedRadBut(ListRadioButtonPas);
            PGeneric.Visibility = Visibility;
            StringToCompare = null;
        }
        private void PSelectionCheck1()//разрешает выбор типа данных очищает поля вывода,ввода
        {
            PSign.IsEnabled = true;
            PUSign.IsEnabled = true;
            PReal.IsEnabled = true;
            PRealF.IsEnabled = true;
            PChar.IsEnabled = true;
            PString.IsEnabled = true;
        }
        void PClear()
        {
            PPrintResult.Clear();
            PPrintResult16.Clear();
        }
        private void EnterNum_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TrainingMode.IsChecked == true)
            {
                Clear();
            }
        }
        private void PEnterNum_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (PTrainingMode.IsChecked == true)
            {
                PClear();
            }
        }
    }
}